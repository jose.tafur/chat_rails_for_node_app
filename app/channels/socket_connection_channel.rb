 class SocketConnectionChannel < ApplicationCable::Channel
  def subscribed
   @uuid = params[:uuid]
   stop_all_streams
   stream_from "socket_connect_#{@uuid}"
   logger.info ">>> Subscribed #{@uuid}!"
  end

  def speak
    logger.info "[ActionCable] received message : #{data['message']}" 
    ActionCable.server.broadcast "socket_connect_#{@uuid}", message: "#{data['message']} from server"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
   end

 end